# nDocumentation

## Getting started
This is the starting point for documentation in n.OS.


## Build System
We use Arch Build system because it is fun to use and is relatively simple.

## nRules
* Be kind to one another
* Be helpful
* Others learn, too
* Everybody starts as a beginner
